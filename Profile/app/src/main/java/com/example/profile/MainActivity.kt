package com.example.profile

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.*
import android.util.Log
import android.view.*
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    private lateinit var etSurname: EditText
    private lateinit var etFirstName: EditText
    private lateinit var etSecondName: EditText
    private lateinit var etAge: EditText
    private lateinit var etHobbies: EditText
    private lateinit var tvWarn: TextView
    private lateinit var btNext: Button
    private lateinit var imBackground: ConstraintLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etSurname = findViewById(R.id.etSurname)
        etFirstName = findViewById(R.id.etFirstName)
        etSecondName = findViewById(R.id.etSecondName)
        etAge = findViewById(R.id.etAge)
        etHobbies = findViewById(R.id.etHobbies)
        tvWarn = findViewById(R.id.tvWarn)
        btNext = findViewById(R.id.btNext)
        imBackground = findViewById(R.id.imBackground)

        val person = PersonData()
        loadData(person)

        etSurname.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                Log.i("MainActivity", "Surname input: $p0")
                person.surname = p0.toString()
            }
        })
        etFirstName.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                Log.i("MainActivity", "FirstName input: $p0")
                person.firstName = p0.toString()
            }
        })

        etSecondName.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                Log.i("MainActivity", "SecondName input: $p0")
                person.secondName = p0.toString()
            }
        })

        etAge.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                Log.i("MainActivity", "Age input: $p0")
                person.age = p0.toString()
            }
        })

        etHobbies.addTextChangedListener(object: TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun afterTextChanged(p0: Editable?) {
                Log.i("MainActivity", "Hobbies input: $p0")
                person.hobbies = p0.toString()
            }
        })

        btNext.setOnClickListener{
            if(person.isNotEmpty()){
                val intent = Intent(this, InfoActivity::class.java)
                intent.putExtra(Constants.firstName, person.firstName)
                intent.putExtra(Constants.secondName, person.secondName)
                intent.putExtra(Constants.surname, person.surname)
                intent.putExtra(Constants.age, person.age)
                intent.putExtra(Constants.hobbies, person.hobbies)
                startActivity(intent)
                //finish()
            }
            else tvWarn.setVisibility(View.VISIBLE)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val pic = listOf(R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4, R.drawable.img5)
        when(item.itemId){
            R.id.next -> {
                val intent = Intent(this, LivedataActivity::class.java)
                startActivity(intent)
            }
            R.id.random -> {
                val rand = Random.nextInt(pic.size)
                imBackground = findViewById(R.id.imBackground)
                imBackground.setBackgroundResource(pic[rand])
            }
            R.id.save -> saveData()
        }
        return true
    }

    private fun saveData(){
        val surname = etSurname.text.toString()
        val firstName = etFirstName.text.toString()
        val secondName = etSecondName.text.toString()
        val age = etAge.text.toString()
        val hobbies = etHobbies.text.toString()

        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.apply{
            putString(Constants.surname, surname)
            putString(Constants.firstName, firstName)
            putString(Constants.secondName, secondName)
            putString(Constants.age, age)
            putString(Constants.hobbies, hobbies)
        }.apply()
    }
    private fun loadData(person: PersonData){
        val sharedPreferences = getSharedPreferences("sharedPrefs", Context.MODE_PRIVATE)
        person.surname = sharedPreferences.getString(Constants.surname, "")
        person.firstName = sharedPreferences.getString(Constants.firstName, "")
        person.secondName = sharedPreferences.getString(Constants.secondName, "")
        person.age = sharedPreferences.getString(Constants.age, "")
        person.hobbies = sharedPreferences.getString(Constants.hobbies, "")

        etSurname.setText(person.surname)
        etFirstName.setText(person.firstName)
        etSecondName.setText(person.secondName)
        etAge.setText(person.age)
        etHobbies.setText(person.hobbies)
    }
}