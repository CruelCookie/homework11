package com.example.profile

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class InfoActivity : AppCompatActivity() {
    private lateinit var tvText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val surname = intent.getStringExtra(Constants.surname)
        val firstName = intent.getStringExtra(Constants.firstName)
        val secondName = intent.getStringExtra(Constants.secondName)
        val age = intent.getStringExtra(Constants.age)
        val hobbies = intent.getStringExtra(Constants.hobbies)

        tvText = findViewById(R.id.tvText)
        tvText.text = when(age?.toInt()){
            in 0..10 -> "Его имя - $surname $firstName $secondName! Хотя сейчас ему и $age, а его интересы - $hobbies, престол все равно займет он..."
            in 11..19 -> "Меня зовут $surname $firstName $secondName. Мне $age лет. В круг моих интересов входит: $hobbies. Пожалуйста, позаботьтесь обо мне"
            in 20..40 -> "Здравствуйте! Сегодняшний гость 'Пусть говорят' - $surname $firstName $secondName, ему $age лет, он увлекается $hobbies. Но сегодня мы узнаем его темные тайны..."
            else -> "Что?!? Говоришь, его зовут $surname $firstName $secondName? Что ему $age лет? Он еще и увлечения себе придумал? $hobbies? Хорошо этот рептилоид себе историю продумал"
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) finish()
        return true
    }
}