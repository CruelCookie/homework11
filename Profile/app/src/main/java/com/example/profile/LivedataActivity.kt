package com.example.profile

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

class LivedataActivity : AppCompatActivity() {
    private lateinit var viewModel: MyViewModel
    private lateinit var btLivedata: Button
    private lateinit var etLivedata: EditText
    private lateinit var tvLivedata: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_livedata)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tvLivedata = findViewById(R.id.tvLivedata)

        viewModel = ViewModelProvider(this).get(MyViewModel::class.java)
        viewModel.currentText.observe(this, Observer {
            tvLivedata.text = it.toString()
            Log.i("LivedataActivity", "${tvLivedata.text}")
        })

        saveText()
    }

    private fun saveText(){
        btLivedata = findViewById(R.id.btLivedata)
        etLivedata = findViewById(R.id.etLivedata)

        btLivedata.setOnClickListener{
            viewModel.currentText.value = etLivedata.getText().toString()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) finish()
        return true
    }
}