package com.example.profile

class PersonData {
    var surname: String? = ""
    var firstName: String? = ""
    var secondName: String? = ""
    var age: String? = ""
    var hobbies: String? = ""

    fun isNotEmpty(): Boolean{
        return (surname != "") and (firstName != "") and (secondName != "") and (age != "") and (hobbies != "")
    }
}