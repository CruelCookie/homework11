package com.example.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MyViewModel: ViewModel() {
    val currentText: MutableLiveData<String> by lazy{
        MutableLiveData<String>()
    }
}